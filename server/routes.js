'use strict';

var UserController = require("./api/user/user.controller");
// var PostController = require("./api/post/post.controller");
// var CommentController = require("./api/comment/comment.controller");
// var AWSController = require("./api/aws/aws.controller");
var express = require("express");
var config = require("./config");
var grabnews = require('./grabnews')

const API_POSTS_URI = "/api/posts";
const API_USERS_URI = "/api/users";
const API_COMMENTS_URI = "/api/comments";
const API_AWS_URI = "/api/aws";
// const HOME_PAGE = "/home.html#!/main";
// const SIGNIN_PAGE = "/home.html#!/signIn";

// module.exports = function (app, db) {
//   );
// }

module.exports = function (app, passport, db) {

// app.all('*', function(req, res, next) {
//        res.header("Access-Control-Allow-Origin", "*");
//        res.header("Access-Control-Allow-Headers", "X-Requested-With");
//        res.header('Access-Control-Allow-Headers', 'Content-Type');
//        next();
// });

var Retrieve = require('./api/user/retrieve.controller')(db);
  app.get('/userFetch', Retrieve.retrieveUser);

  app.put('/updateDetails/:username', Retrieve.updateUser);

app.get('/profilemanagement', Retrieve.profileManage);


app.get('/user/auth', function(req, res) {
    if (req.user) {
      res.status(200).json({ user: req.user });
    }
    else {
      res.sendStatus(401);
    }
  });

  app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });


  app.get('/grabnews', function(req, res){
    

        // rss items
        /*
        console.log("***** " + item.title + "***** ");
        console.log(item.link);
        console.log(item.author);
        console.log("***** ******** ***** ");
        */
    /*

    grabnews.cna().then(function(stream){ 
      var item;
      
      while (item = stream.read()) {
        var newsArticle = {
          title: item.title,
          desc: item.description,
          link: item.link
        }
        articles.push(newsArticle);
      }

      
    }).catch(function(err){
      console.log(err)
    })*/

    
    grabnews.news().then(function(result){ 
      // console.log(">>>> " + result);
      console.log(result.length);
      res.json(result);
    }).catch(function(err){
      console.log(err)
    })

  })
    // Posts API
    // app.get(API_POSTS_URI, isAuthenticated, PostController.list);
    // app.get(API_POSTS_URI + '/image/:url', isAuthenticated, PostController.showImage);
    // app.get(API_POSTS_URI + '/me', isAuthenticated, PostController.me);
    // app.post(API_POSTS_URI, isAuthenticated, PostController.create);
    // app.get(API_POSTS_URI + '/:id', isAuthenticated, PostController.get);
    // app.post(API_POSTS_URI + '/:id', isAuthenticated, PostController.update);
    // app.delete(API_POSTS_URI + '/:id', isAuthenticated, PostController.remove);
    // app.post(API_POSTS_URI + '/:id/like', isAuthenticated, PostController.likePost);
    // app.get(API_POSTS_URI + '/:postId/comments', isAuthenticated, CommentController.byPosts);

    // // Users API
    // app.get(API_USERS_URI, isAuthenticated, UserController.list);
    // app.post(API_USERS_URI, isAuthenticated, UserController.create);
    // app.get(API_USERS_URI+ '/:id', isAuthenticated, UserController.get);
    // app.get(API_USERS_URI + '/:id/posts', isAuthenticated, PostController.listByUser);
    // app.post(API_USERS_URI + '/:id', isAuthenticated, UserController.update);
    // app.delete(API_USERS_URI + '/:id', isAuthenticated, UserController.remove);
    // app.get("/api/user/view-profile", isAuthenticated, UserController.profile);
    // app.get("/api/user/social/profiles", isAuthenticated, UserController.profiles);

    // Comments API
    // app.post(API_COMMENTS_URI, isAuthenticated, CommentController.create);
    // app.delete(API_COMMENTS_URI + '/:id', isAuthenticated, CommentController.remove);

    // app.get("/protected/", isAuthenticated, function(req, res){
    //     if(req.user == null){
    //         res.redirect(SIGNIN_PAGE);
    //     }
    // })

    // // AWS policy API
    // app.get(API_AWS_URI + '/createS3Policy', isAuthenticated, PostController.list);
    // app.post(API_AWS_URI + '/s3-policy', isAuthenticated, AWSController.getSignedPolicy);

    // app.use(express.static(__dirname + "/../client/"));

    // app.post("/change-password", isAuthenticated, UserController.changePasswd);

    // app.get("/api/user/get-profile-token", UserController.profileToken);
    // app.post("/api/user/change-passwordToken", UserController.changePasswdToken);

    app.post('/register', UserController.register);


  

    // app.post("/login", passport.authenticate("local", {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: "/",
    //     failureFlash : true
    // }));

app.post('/login', passport.authenticate('local'), function(req, res) {
    res.status(200).json({user: req.user});
  });

  app.get('/user/auth', function(req, res) {
    if (req.user) {
      res.status(200).json({ user: req.user });
    }
    else {
      res.sendStatus(401);
    }
  });

  app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
  });

    // app.post("/reset-password", UserController.resetPasswd);

    // app.get('/home', isAuthenticated, function(req, res) {
    //     res.redirect('..' + HOME_PAGE);
    // });


    // app.get("/oauth/google", passport.authenticate("google", {
    //     scope: ["email", "profile"]
    // }));

    // app.get("/oauth/google/callback", passport.authenticate("google", {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: SIGNIN_PAGE
    // }));    //This gives out

    // app.get("/oauth/facebook", passport.authenticate("facebook", {
    //     scope: ["email", "public_profile"]
    // }));    //This receives it back

    // app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: SIGNIN_PAGE,
    //     failureFlash : true
    // }));

    // app.get('/oauth/linkedin',
    //     passport.authenticate('linkedin', { state: 'SOME STATE'  }),
    //     function(req, res){
    //         // The request will be redirected to LinkedIn for authentication, so this
    //         // function will not be called.
    //     });

    // app.get('/oauth/linkedin/callback', passport.authenticate('linkedin', {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: SIGNIN_PAGE,
    //     failureFlash : true
    // }));

    // app.get('/oauth/wechat', passport.authenticate('wechat'));

    // app.get('/oauth/wechat/callback', passport.authenticate('wechat', {
    //     failureRedirect: SIGNIN_PAGE,
    //     successReturnToOrRedirect: HOME_PAGE,
    //     failureFlash : true
    // }));

    // app.get('/oauth/twitter', passport.authenticate('twitter'));

    // // handle the callback after twitter has authenticated the user
    // app.get('/oauth/twitter/callback',
    //     passport.authenticate('twitter', {
    //         successRedirect : HOME_PAGE,
    //         failureRedirect : SIGNIN_PAGE
    //     }));

    app.get("/status/user", function (req, res) {
        var status = "";
        if(req.user) {
            status = req.user.email;
        }
        console.info("status of the user --> " + status);
        res.send(status).end();
    });

    // app.get("/logout", function(req, res) {
    //     req.logout();             // clears the passport session
    //     req.session.destroy();    // destroys all session related data
    //     res.send(req.user).end();
    // });


    // function isAuthenticated(req, res, next) {
    //     if (req.isAuthenticated())
    //         return next();
    //     res.redirect(SIGNIN_PAGE);
    // }
    // app.use(function(req, res, next){
    //     if(req.user == null){
    //         res.redirect(SIGNIN_PAGE);
    //     }
    //     next();
    // });

};
