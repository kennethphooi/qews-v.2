var retrieveUser = function (db) {
    return function (req, res) {
        console.log("retrieveUer");
        db.User
            .findAll({
                where: {
                    $or: [
                        { username: { $like: "%" + req.query.searchString + "%" } }
                    ]
                },
            })
            .then(function (users) {
                console.log('then')
                res
                    .status(200)
                    .json(users);
            })
            .catch(function (err) {
                console.log(err);
                res
                    .status(500)
                    .json(err);
            });
    }
}

var profileManage = function (db) {
    return function (req, res) {
        db.User
            .findOne({
                where: {
                    username: req.user.username
                }
            })
            .then(function (result) {
                if (!result) {
                    res
                        .status(400)
                        .end();
                    return
                }
                res
                    .status(200)
                    .json(result);
            })
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            })

    }
}





var updateUser = function (db) {
    return function (req, res) {
        db.User
            .update(
            req.body,
            {
                where: {
                    username: req.params.username
                }
            }
            )
            .then(function (users) {
                console.log("success " + JSON.stringify(users));
                res.json(users);
            })

            .catch(function (err) {
                console.log("failure" + JSON.stringify((err)));
                res
                    .status(500)
                    .json({ error: true });
            });

    };
}

module.exports = function (db) {
    return {
        retrieveUser: retrieveUser(db),
        updateUser: updateUser(db),
        profileManage: profileManage(db),
    }
};