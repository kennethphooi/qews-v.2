var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('users', {
        id: {
            type: Sequelize.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        username: {
            type : Sequelize.STRING(100),
            allowNull: false
        },
        // created_at: {
        //     type: Sequelize.DATE,
        //     allowNull: false,
        // },
        // updated_at: {
        //     type: Sequelize.DATE,
        //     allowNull: false,
        // },  
        email: {
            type : Sequelize.STRING(100),
            allowNull: false,
            unique: true,
        },
        name_first: {
            type: Sequelize.STRING(200),
            allowNull: false
        },
        name_last: {
            type: Sequelize.STRING(200),
            allowNull: false
        },
        encrypted_password: {
            type: Sequelize.STRING(64),
            allowNull: false
        },
        admin: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
    },
    {
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        freezeTableName: true,
  });

};