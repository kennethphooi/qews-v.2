
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");		
var passport = require("passport");
var session = require('express-session');
var flash = require('connect-flash');


const config = require('./config');


const NODE_PORT = process.env.PORT || process.env.NODE_PORT || config.PORT || 3000;		


const CLIENT_FOLDER = path.join(__dirname, '/../client');  
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');


var app = express();






const db = require('./db');

app.use(flash());
app.use(express.static(CLIENT_FOLDER));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(session({
	secret: config.SECRET,
	resave: true,
	saveUninitialized: true,
}));

app.use(passport.initialize());
app.use(passport.session());

require('./auth.js')(app, passport);
require("./routes")(app, passport, db);


// app.use(function(req, res) {
// 	//res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
// 	res.sendStatus(404);
// });





// app.use(function(err, req, res, next) {
// 	res.sendStatus(500);
// 	// res.status(500).sendFile(path.join(MSG_FOLDER + '/500.html'));
// });

app.listen(NODE_PORT, function() {
	console.log("Server running at http://localhost:" + NODE_PORT);
});
