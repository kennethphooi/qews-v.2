var FeedParser = require('feedparser');
var request = require('request'); // for fetching the feed

var news_site = ['http://www.zaobao.com/ssi/rss/sp.xml', 'http://www.channelnewsasia.com/rssfeeds/8396082'];
var articles = [];

news_site.forEach(function(item){
    console.log(item);
    var req = request(item)
    var feedparser = new FeedParser();

    req.on('error', function (error) {
    // handle any request errors
    });

    req.on('response', function (res) {
    var stream = this; // `this` is `req`, which is a stream

    if (res.statusCode !== 200) {
        this.emit('error', new Error('Bad status code'));
    }
    else {
        stream.pipe(feedparser);
    }
    });

    feedparser.on('error', function (error) {
    // always handle errors
    });

    feedparser.on('readable', function () {
        // This is where the action is!
        var stream = this; // `this` is `feedparser`, which is a stream
        var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
        var item;
        //console.log(meta);
        while (item = stream.read()) {
            console.log(item.title);
            articles.push(item);
        }
    });
})


