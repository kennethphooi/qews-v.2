var FeedParser = require('feedparser');
var request = require('request'); // for fetching the feed
//var news_site = ['http://www.zaobao.com/ssi/rss/sp.xml'];


var news_site = ['http://www.channelnewsasia.com/rssfeeds/8396082',
        // 'http://www.zaobao.com/ssi/rss/sp.xml', 
        // 'http://www.straitstimes.com/news/singapore/rss.xml',
        ];

// todayonline RSS: 'http://www.todayonline.com/feed/singapore'
// channelnewsasia RSS: 'http://www.channelnewsasia.com/rssfeeds/8396082'
// straitstimes RSS: 'http://www.straitstimes.com/news/singapore/rss.xml'
// sequelize to news_site then for loop 
// grab news feed
// sequelize insert to the news_article by date
//for  from news site using sequelize
function news(){
  return new Promise(function (resolve, reject) {
    var articles = [];
    var x = 1;
    news_site.forEach(function(item){
        console.log(item);
        var req = request(item)
        var feedparser = new FeedParser();

        req.on('error', function (error) {
          // handle any request errors
        });
      
        feedparser.on('end', done);

        req.on('response', function (res) {
          var stream = this; // `this` is `req`, which is a stream

          if (res.statusCode !== 200) {
              this.emit('error', new Error('Bad status code'));
          }
          else {
              stream.pipe(feedparser);
          }
        });

        feedparser.on('error', function (error) {
          // always handle errors
        });

        feedparser.on('readable', function () {
            // This is where the action is!
            var stream = this; // `this` is `feedparser`, which is a stream
            var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
            var item;
            while (item = stream.read()) {
                //console.log(item.title);
                // console.log(item.link);
                // console.log(item.summary);
                articles.push(item);
            }
        });

        function done(err) {
          // console.log(news_site.length);
          
          
          if(x == news_site.length){

            resolve(articles);
          }
          x++;  
        }
        
    })
    
    /*
    while(x != news_site.length){
      console.log("still looping?");
      resolve(articles);
    }*/
      
  })
  
}

function getFeed(url, stream){
    console.log(url);
    var req2 = request(url)
    var feedparser2 = new FeedParser();

    req2.on('error', function (error) {
      // handle any request errors
      reject('error')
    });

    req2.on('response', function (res) {
      
      if (res.statusCode !== 200) {
        this.emit('error', new Error('Bad status code'));
      }
      else {
        stream.pipe(feedparser2);
      }
    });

    feedparser2.on('error', function (error) {
      // always handle errors
    });

    feedparser2.on('readable', function () {
      // This is where the action is!
      var stream = this; // `this` is `feedparser`, which is a stream
      var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
      resolve(stream);
    });
}

module.exports = {
  news: news
}