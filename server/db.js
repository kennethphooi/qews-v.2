// Read configurations
var config = require('./config');

// Load Sequelize package
var Sequelize = require("sequelize");

// Create Sequelize DB connection
var sequelize = new Sequelize(
	'qews',
	config.MYSQL_USERNAME,
	config.MYSQL_PASSWORD,
	{
		host: config.MYSQL_HOSTNAME,
		port: config.MYSQL_PORT,
		logging: config.MYSQL_LOGGING,
		dialect: 'mysql',
		pool: {
			max: 5,
			min: 0,
			idle: 10000,
		},
	}
);


const User = sequelize.import('./models/users');
const News = sequelize.import('./models/news');
const Comments = sequelize.import('./models/comments');

// User.hasMany(UserNews, { foreignKey: 'userid' });
// DeptManager.belongsTo(Employee, { foreignKey: 'emp_no' });


module.exports = {

  User: User,
  News: News,
  Comments: Comments,
};
