
var LocalStrategy = require("passport-local").Strategy;
var bcrypt   = require('bcryptjs');

var User = require("./db").User;
var config = require("./config");

//Setup local strategy
module.exports = function (app, passport) {
    function authenticate(username, password, done) {

        User.findOne({
            where: {
                email: username
            }
        }).then(function(result) {
            console.log("User Exists");
            if(!result){
                return done(null, false);
            }else{
                if(bcrypt.compareSync(password, result.encrypted_password)){
                    console.log("User password match");
                    var whereClause = {};
                    whereClause.email = username;
                    User
                    .update({ reset_password_token: null},
                    {where:  whereClause});
                    console.log("line 29 auth.js"   + result.admin);
                    return done(null, result);
                    
                }else{
                    console.log("User password does not match");
                    return done(null, false);
                }
            }
        }).catch(function(err){
            console.log("User does not Exists");
            return done(err, false);
        });
    
    }

    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        console.info("serial to session");
        console.log(user);
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        console.log("deserial");
        console.log(user);
        User.findOne({
            where: {
                email: user.email
            }
        }).then(function(result) {
            if(result){
                done(null, user);
            }
        }).catch(function(err){
            done(err, user);
        });
})
};
