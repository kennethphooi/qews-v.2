(function() {
    angular
        .module('qews', ["ui.router", "ngFlash", "ngSanitize", "ngProgress", "ngAnimate"]);
})();