(function() {
    angular
        .module("qews")
        .controller("MainCtrl", MainCtrl);

    MainCtrl.$inject = ['$state', 'Flash', 'ngProgressFactory', 'GrabNewsSvc', '$scope', 'user']

    function MainCtrl($state, Flash, ngProgressFactory, GrabNewsSvc, $scope, user) {
        var vm = this;

        vm.progressbar = ngProgressFactory.createInstance();
        vm.user = user;
        vm.greetings =
        vm.messages = [];
        vm.newsout = [];
        vm.init = init;

        vm.moretext = [];
        vm.moretext2 = [];
        // vm.moretext = "";
        vm.moretextcol = ["What else?", "Anymore anot?", "Tell me more!", "Share more!", "Next!", "Skip!", "Somemore?", "What's next?", "News me up!"];

        vm.shootnews = shootnews;

        $scope.greeting = "Wah " + user.name_first + "! Glad to have you back. Let's see what we have going on in sunny Singapore today...";
        init();




        //What else?, Anymore anot?, Tell me more!, Share more!, Next!, Skip! Somemore? What's next? 

        // function moretextgenerator() {
        //     var i = Math.round(Math.random() * 8);
        //     console.log(i);
        //     return vm.moretextcol[i];
        // }





        function init() {
            GrabNewsSvc.grabNews()
                .then(function(articles) {
                    // console.log(articles);
                    vm.messages = articles.data;
                    // console.log(vm.messages);
                    // console.log(vm.messages[13].description)
                })
        };




        // function shootnews() {
        //     vm.newsout.push(vm.messages.shift());

        //     vm.moretext.push(vm.moretext2.shift());
        //     //console.log($("#bottomPart"));
        //     scrollDown();
        //     function moretextgenerator() {
        //         var i = Math.round(Math.random() * 8);
        //         console.log(i);
        //         return vm.moretextcol[i];
        //     }
   

        // }

        function shootnews() {

            var news = {};
            news = {
                article: vm.messages.shift(),
                moretext: moretextgenerator()
            };

            vm.newsout.push(news);
            console.log(vm.newsout);
            //console.log($("#bottomPart"));
            scrollDown();
            function moretextgenerator() {
                var i = Math.round(Math.random() * 8);
                console.log(i);
                return vm.moretextcol[i];
            }
   

        }



        //news is currently displayed on the frontend through "ctrl.messages", which is vm.messages
        //need to create a new array (vm.newsout) which is populated by 1 everytime a click is executed


        function scrollDown() {
            var focusBottom = document.getElementById("adobewordpress");
            focusBottom.scrollTop = focusBottom.scrollHeight;
        }

        jQuery("input").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                jQuery('form.chat input[type="submit"]').click();
            }
        });
        jQuery('form.chat input[type="submit"]').click(function(event) {
            event.preventDefault();
            var message = jQuery('form.chat input[type="text"]').val();
            if (jQuery('form.chat input[type="text"]').val()) {
                var d = new Date();
                var clock = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var currentDate =
                    (('' + day).length < 2 ? '0' : '') + day + '.' +
                    (('' + month).length < 2 ? '0' : '') + month + '.' +
                    d.getFullYear() + '&nbsp;&nbsp;' + clock;
                jQuery('form.chat div.messages').append('<div class="message"><div class="myMessage"><p>' + message + '</p><date>' + currentDate + '</date></div></div>');
                setTimeout(function() {
                    jQuery('form.chat > span').addClass('spinner');
                }, 100);
                setTimeout(function() {
                    jQuery('form.chat > span').removeClass('spinner');
                }, 2000);
            }
            jQuery('form.chat input[type="text"]').val('');
            scrollDown();
        });

        /* DEMO */
        if (parent == top) {
            jQuery('a.article').show();
        }




        vm.visible = true;
        vm.expandOnNew = true;


    }



})();