(function() {
  angular
    .module("qews")
    .controller("SearchCtrl", SearchCtrl);

  SearchCtrl.$inject = ['PassportSvc', '$state'];

  function SearchCtrl(PassportSvc, $state) {
    var vm = this;

    vm.searchString = '';
    vm.result = null;
    vm.showUser = false;
    vm.shift = shift;

    // Exposed functions --------------------------------------------------------------------------
    // Exposed functions can be called from the view.
    vm.search = search;


    // Function declaration and definition --------------------------------------------------------
    // The init function initializes view
    function init() {
      // We call DeptService.retrieveDeptDB to handle retrieval of department information. 
      // The data retrieved from this function is used to populate search.html. Since we are 
      // initializing the view, we want to display all available departments, thus we ask 
      // service to retrieve '' (i.e., match all)
      PassportSvc
        .retrieveUser('')
        .then(function (results) {
          // The result returned by the DB contains a data object, which in turn contains the 
          // records read from the database
          vm.users = results.data;
          console.log("here2");
        })
        .catch(function (err) {
          // We console.log the error. For a more graceful way of handling the error, see
          // register.controller.js
          console.log("error ");
          console.log(err);
        });
    }

    // The search function searches for departments that matches query string entered by user. The query string is
    // matched against the department name and department number alike.
    function search() {
      vm.showUser = false;
      console.log("here4")
      PassportSvc
        // we pass contents of vm.searchString to service so that we can search the DB for this 
        // string
        .retrieveUser(vm.searchString)
        .then(function (results) {
          // The result returned by the DB contains a data object, which in turn contains the 
          // records read from the database
          vm.users = results.data;
          console.log("here3");
          console.log(results);
        })
        .catch(function (err) {
          // We console.log the error. For a more graceful way of handling the error, see
          // register.controller.js
          console.log("error ");
          console.log(err);
        });
    }

    function shift(username) {
            console.log("STATE")
            $state.go("edit", {
                username: username
            })
        }

  }
})();