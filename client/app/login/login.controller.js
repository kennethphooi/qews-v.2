(function () {
  angular
    .module('qews')
    .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = ['PassportSvc', '$state', "Flash"];

  function LoginCtrl(PassportSvc, $state, Flash) {
    var vm = this;
    vm.inputType = 'password';
    vm.user = {
      username: '',
      password: '',
    }
    vm.msg = '';
    vm.passwordCheckBox = false;
    vm.hideShowPassword = function () {
      if (vm.inputType == 'password')
        vm.inputType = 'text';
      else
        vm.inputType = 'password';
    };
    vm.login = login;

    function login() {
      PassportSvc.login(vm.user)
        .then(function (result) {
          console.log("User: ", result);
          $state.go('main');
          return true;
        })
        .catch(function (err) {
          console.log("incorrect username")
          Flash.create('danger', "Mayday! We've got a hacker! Try harder.")
          vm.msg = 'Invalid Username or Password!';
          vm.user.username = vm.user.password = '';
          return false;
        });
    }
  }
})();