(function () {
    angular
        .module("qews")
        .config(qewsAppConfig);
    qewsAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function qewsAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state("main", {
                    url: "/main",
                    data: {
                        bodyClass: 'homeclass'
                    },
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/main/main.html",
                controller: "MainCtrl as ctrl",
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })
          
             .state("signIn", {
                    url: "/signIn",
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/login/login.html",
                controller: "LoginCtrl",
                controllerAs: "ctrl"
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })

        .state("signUp", {
                    url: "/signUp",
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/register/register.html",
                controller: "RegisterCtrl as ctrl",
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })

        .state("modifyNews", {
                    url: "/modifyNews",
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/news/modifynews.html",
                controller: "ModifyNewsCtrl",
                controllerAs: "ctrl"
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })
            
       .state("admin", {
                    url: "/admin",
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/search/search.html",
                controller: "SearchCtrl",
                controllerAs: "ctrl"
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })

        .state("edit", {
                    url: "/edit/:username",
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/edit/edit.html",
                controller: "EditCtrl",
                controllerAs: "ctrl"
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })

        .state("logout", {
                    url: "/logout",
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/logout/logout.html",
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })

        .state("profile", {
                    url: "/manageprofile",
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/profile/profile.html",
                controller: "ProfileCtrl as ctrl",
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })

        .state("forum", {
                    url: "/forum",
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/forum/forum.html",
                controller: "ForumCtrl as ctrl",
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        }) 

         .state("about", {
                    url: "/about",
                     data: {
                        bodyClass: 'aboutclass'
                    },
                    views: {
                        'menu': {
                    templateUrl: "app/menu/menu.html",
                    controller: "MenuCtrl as ctrl",
                },
                'content': {
                templateUrl: "app/about/about.html",
            }
                },
                
                
                resolve: {
                user: function(PassportSvc) {
                    return PassportSvc.userAuth()
                .then(function(result) {
                    return result.data.user;
                })
                .catch(function(err) {
                    return '';
                });
             }
            },
        })
            
            $urlRouterProvider.otherwise("/main");
    }
})();