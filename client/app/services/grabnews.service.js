(function () {
  angular
    .module('qews')
    .service('GrabNewsSvc', GrabNewsSvc);

  GrabNewsSvc.$inject = ["$http"];

  function GrabNewsSvc($http) {
    var svc = this;

    
    svc.grabNews = grabNews;

    function grabNews() {
      return $http.get(
        '/grabnews'
      );
    }


  }

})();


// http://smmry.com/api (API for summarizing news)