(function () {
  angular
    .module('qews')
    .service('PassportSvc', PassportSvc);

  PassportSvc.$inject = ["$http"];

  function PassportSvc($http) {
    var svc = this;

    svc.login = login;
    svc.userAuth = userAuth;
    svc.retrieveUser = retrieveUser;
    svc.updateDetails = updateDetails;
    svc.profileManage = profileManage;

    function userAuth() {
      return $http.get(
        '/user/auth',
      );
    }

    function login(user) {
      return $http.post(
        '/login',
        user
      );
    }

    function retrieveUser(searchString) {
      return $http.get(
        '/userFetch', {
          params: {
            'searchString': searchString
          }
        });
    }

    function profileManage() {
      return $http.get (
        '/profilemanagement', 
      )
    }

     function updateDetails(username, name_first, name_last) {
            return $http({
                method: 'PUT'
                , url: '/updateDetails/' + username
                , data: {
                    username: username,
                    name_first: name_first,
                    name_last: name_last,
                }
            });
        }

  }

})();
